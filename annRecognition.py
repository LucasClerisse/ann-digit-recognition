# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 22:43:18 2017

@author: lucas
"""

# Importing packages needed

import scipy.special
import matplotlib.pyplot as plt
import numpy as np

# Constant for the network creation

INPUT_NODES = 784
HIDDEN_NODES = 200
OUTPUT_NODES = 10
LEARNING_RATE = 0.1
EPOCH = 10

# Class representing the network

class network:
    
    def __init__(self, inputN, hiddenN, outputN, learningRate):
        print("Initializing network...")
        self.inodes = inputN
        self.hnodes = hiddenN
        self.onodes = outputN
        self.lr = learningRate
        self.wih = np.random.normal(0.0, pow(self.inodes, -0.5), (self.hnodes, self.inodes))
        self.who = np.random.normal(0.0, pow(self.hnodes, -0.5), (self.onodes, self.hnodes))
        
        self.activation_function = lambda x: scipy.special.expit(x)
        
        pass
        pass
    
    def train(self, inputs_list, targets_list):
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)
        output_errors = targets - final_outputs
        hidden_errors = np.dot(self.who.T, output_errors) 
        self.who += self.lr * np.dot((output_errors * final_outputs * (1.0 - final_outputs)), np.transpose(hidden_outputs))
        self.wih += self.lr * np.dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)), np.transpose(inputs)) 
        pass
    
    def query(self, inputs_list):
        inputs = np.array(inputs_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)      
        return final_outputs
    
def main():
    
    ANN = network(INPUT_NODES, HIDDEN_NODES, OUTPUT_NODES, LEARNING_RATE)
    trainingFile = open("./mnist_train.csv", 'r')
    trainingSet = trainingFile.readlines()
    trainingFile.close()
    
    for i in range(EPOCH):
        for sample in trainingSet:
            allValues = sample.split(',')
            inputs = (np.asfarray(allValues[1:]) / 255.0 * 0.99) + 0.01
            targets = np.zeros(OUTPUT_NODES) + 0.01
            targets[int(allValues[0])] = 0.99
            ANN.train(inputs, targets)
            pass
        pass
    print("Training phase terminated !")
    ## Testing efficacity
    test_data_file = open("mnist_test.csv", 'r')
    test_data_list = test_data_file.readlines()
    test_data_file.close()
    
    scorecard = []

    for record in test_data_list:
        all_values = record.split(',')
        correct_label = int(all_values[0])
        inputs = (np.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        outputs = ANN.query(inputs)
        label = np.argmax(outputs)
        if (label == correct_label):
            scorecard.append(1)
        else:
            scorecard.append(0)
            pass  
        pass
    scorecard_array = np.asarray(scorecard)
    print ("performance = ", scorecard_array.sum() / scorecard_array.size)
        
      
if __name__ == '__main__':
    main()

