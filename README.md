# ANN Digit recognition #

This is one of my first neural network created after reading "Make your own neural network"

### What is this repository for? ###

* This neural network can :
	* Recognize which digit you drew
	* Learn from your example

### Credits ###

Thanks to the book "Make your own neural Network" which gave me a good overview of ANN